# The recommended compiler flags for the Raspberry Pi
CCFLAGS=-Ofast -mfpu=vfp -mfloat-abi=hard -march=armv6zk -mtune=arm1176jzf-s
#CCFLAGS=

# define all programs
PROGRAMS = main 

#SOURCES = ${PROGRAMS:=.cpp}

CPP_FILES := $(wildcard src/*.cpp)
OBJ_FILES := $(addprefix obj/,$(notdir $(CPP_FILES:.cpp=.o)))

obj/%.o: src/%.cpp
	g++ $(CCFLAGS) -Wall -I../ -c -o $@ $<
#	g++ $(CC_FLAGS) -Wall -I../ -lrf24-bcm -c -o $@ $<

all: ${PROGRAMS}

main: $(OBJ_FILES)
	g++ $(LD_FLAGS) -lrf24-bcm -L./lib -lhiredis -o $@ $^

#${PROGRAMS}: ${SOURCES}
#	g++ ${CCFLAGS} -Wall -I../ -lrf24-bcm $@.cpp -o $@

clean:
	rm -rf $(PROGRAMS)
	rm -rf ${OBJ_FILES}

.PHONY: install
