#include "stdint.h"
#include "DBHandler.hpp"
#include <iostream>
#include <sstream>

namespace freerooms
{

DBHandler::DBHandler():
        _redisCtx(0)
{
}

DBHandler::~DBHandler()
{
    if(_redisCtx)
    {
        std::cout << "Freeing redis ctx..";
        redisFree(_redisCtx);
        _redisCtx = 0;
    }
    else
    {
        std::cout << "Not freeing redis ctx..";
    }
}

void DBHandler::pushValue(uint8_t iValue,uint8_t iPipe)
{
    //timestamp
    time_t t = time(0);   // get time now
    size_t aTs = (size_t)t;

    std::stringstream aCommand;

    std::stringstream aScore;
    aScore << aTs;
    std::stringstream aValue;
    aValue << aTs << ":" << (size_t)iValue;

    aCommand << "ZADD sensors:sensor_0_" << (size_t)iPipe << " " << aScore.str() << " " << aValue.str();

    std::cout << "redis cmd: " << aCommand.str() << std::endl;

    redisReply* aReply=0;

    aReply = (redisReply*)redisCommand(_redisCtx,aCommand.str().c_str());
    if(!aReply)
    {
        printf( "Error\n");
    }
    else
    {
        if ( aReply->type == REDIS_REPLY_ERROR )
        {
                printf( "Error: %s\n", aReply->str );
        }
        freeReplyObject(aReply);
    }
    sleep(1);
}

void DBHandler::init()
{
    std::cout << "DBHandler initializing.." << std::endl;

    redisReply *reply;
    const char *hostname = "127.0.0.1";
    int port = 6379;

    struct timeval timeout = { 1, 500000 }; // 1.5 seconds
    _redisCtx = redisConnectWithTimeout(hostname, port, timeout);
    if (_redisCtx == 0 || _redisCtx->err)
    {
       if (_redisCtx)
       {
           printf("Connection error: %s\n", _redisCtx->errstr);
           redisFree(_redisCtx);
           _redisCtx = 0;
       }
       else
       {
           printf("Connection error: can't allocate redis context\n");
       }
    }
    /* PING server */
    reply = (redisReply*)redisCommand(_redisCtx,"PING");
    printf("PING: %s\n", reply->str);
    freeReplyObject(reply);

    std::cout << "DBHandler init OK" << std::endl;
}

}
