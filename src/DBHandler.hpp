#pragma once

extern "C"
{
#include "hiredis.h"
}

namespace freerooms
{

class DBHandler
{
public:
    DBHandler();
    ~DBHandler();
    void init();
    void pushValue(uint8_t iValue,uint8_t iPipe);

    redisContext* _redisCtx;
private:
};

}
