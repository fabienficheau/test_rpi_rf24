#include "Looper.hpp"
#include <iostream>
#include <sstream>
#include <ctime>

using namespace std;

namespace freerooms
{

Looper::Looper():
        _dbHandler(),_rfHandler()
{
}

void Looper::init()
{
    _dbHandler.init();
    _rfHandler.init();

}

void Looper::doLoop()
{
    uint8_t fromPipe = 0;
    uint8_t toRead = 0;
    while(1)
    {
        _rfHandler.read(toRead,fromPipe);
        printf("read: %i from pipe: %i \n",toRead,fromPipe);
        _dbHandler.pushValue(toRead,fromPipe);
    }
}

}
