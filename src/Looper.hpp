#pragma once
#include "RFHandler.hpp"
#include "DBHandler.hpp"

namespace freerooms
{

class Looper
{
public:
    Looper();
    void init();
    void doLoop();
private:
    DBHandler _dbHandler;
    RFHandler _rfHandler;
};

}
