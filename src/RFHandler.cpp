#include "RFHandler.hpp"
#include "DBHandler.hpp"
#include <iostream>

namespace freerooms
{

RFHandler::RFHandler():
    _radio(RPI_V2_GPIO_P1_22, RPI_V2_GPIO_P1_24, BCM2835_SPI_SPEED_8MHZ)
{
}

void RFHandler::read(uint8_t& oRead, uint8_t& oPipe)
{
    if(_radio.available(&oPipe))
    {
        {
            uint8_t aSize = _radio.getPayloadSize();
            printf("asize: %i", aSize);
            char aBug[32];
            memset(aBug,0,32);
            _radio.read(&aBug,sizeof(aBug));
           printf("read: %x from pipe: %i, len: %i \n",aBug[0],oPipe,sizeof(aBug));
        }
    }
    return;
}

void RFHandler::init()
{
    std::cout << "RFHandler initializing.." << std::endl;

    _radio.begin();
    _radio.setRetries(15, 15);
    _radio.setDataRate(RF24_250KBPS);
    _radio.setPALevel(RF24_PA_MAX);
    _radio.setCRCLength(RF24_CRC_8);
    _radio.setRetries(15,15);

    _radio.openReadingPipe(1, ffpipe[0]);
    _radio.openReadingPipe(2, ffpipe[1]);
    _radio.openReadingPipe(3, ffpipe[2]);

    _radio.startListening();
//    _radio.printDetails();

    std::cout << "RFHandler init OK" << std::endl;
}

}
