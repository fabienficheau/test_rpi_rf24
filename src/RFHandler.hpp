#pragma once
#include <RF24/RF24.h>

namespace freerooms
{

class DBHandler;

const uint64_t ffpipe[3] = { 0xE8E8F0F0E1LL, 0xE8E8F0F0E2LL, 0xE8E8F0F0E3LL };

class RFHandler
{
public:
    RFHandler();
    void init();
    void read(uint8_t&,uint8_t&);
private:
    RF24 _radio;
};

}
