#include <cstdlib>
#include <iostream>
#include "Looper.hpp"

using namespace freerooms;

int main(int argc,char** argv)
{
    Looper aLooper;
    aLooper.init();
    aLooper.doLoop();

    return 0;
}
